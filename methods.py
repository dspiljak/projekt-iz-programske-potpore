import matplotlib.pyplot as plt
import cmath
import numpy
import math


def _is_reachable(start_node, target_node):
    """
    Formula that calculates if 2 nodes are reachable
    :param start_node: starting node
    :param target_node: target node
    :return: True if reachable, False otherwise
    """
    return (target_node[1] - start_node[1]) * (target_node[1] - start_node[1] + target_node[0] - start_node[0]) <= 0


def _find_all_paths(start_node, target_node, current_path=[], all_paths=[]):
    """ Method that finds all paths from start node to target node
    :param start_node: Starting node
    :param target_node: Target node
    :param current_path: Array that contains all nodes from the start
    :param all_paths: Array containing all found paths
    :return: Array containing all found paths
    """

    current_path.append(start_node)

    if start_node == target_node:
        all_paths.append(current_path.copy())

    elif not _is_reachable(start_node, target_node):
        # it's impossible to get from current node to target node
        return

    else:
        # go to left child
        _find_all_paths((start_node[0] - 1, start_node[1]), target_node, current_path, all_paths)
        current_path.remove((start_node[0] - 1, start_node[1]))

        # go to right child
        _find_all_paths((start_node[0] - 1, start_node[1] + 1), target_node, current_path, all_paths)
        current_path.remove((start_node[0] - 1, start_node[1] + 1))

        return all_paths


def get_all_paths(start_node, target_node):
    """ A wrapper for find_all_paths method
    :param start_node: Starting node
    :param target_node: Target node
    :return: Array containing all found paths
    """
    if start_node[0] < 1 or start_node[1] < 1 or target_node[0] < 1 or target_node[1] < 1:
        raise ValueError('Index must be greater than 0')
    all_paths = _find_all_paths(start_node, target_node, [], [])
    if all_paths is None:
        raise Exception('No paths found!')
    return all_paths


def _get_possible_indexes(start_row, coin):
    """
    Calculates all possible indexes given the coin in highest row
    :param start_row: starting row
    :param coin: Coin with the highest row number
    :return: indexes
    """

    # (coin[1] - x) * (coin[1] - x + coin[0] - start_row) <= 0
    # x**2 + (-2 * coin[1] - coin[0] + start_row) * x + coin[1] * (coin[1] + coin[0] - start_row) <= 0
    a = 1
    b = -2 * coin[1] - coin[0] + start_row
    c = coin[1] * (coin[1] + coin[0] - start_row)

    # The Discriminant
    d = (b ** 2) - (4 * a * c)

    # The Solutions
    solution1 = (-b - cmath.sqrt(d)) / (2 * a)
    solution2 = (-b + cmath.sqrt(d)) / (2 * a)

    if numpy.iscomplex(solution1):
        return []

    else:
        start = math.ceil(solution1.real)
        if math.ceil(solution1.real) < 1:
            start = 1
        return list(range(start, math.floor(solution2.real) + 1))


def _find_starting_indexes(start_row, coins):
    """
    Finds all possible indexes to start depending on position of coins
    :param start_row: Starting row
    :param coins: Array of coins
    :return: Nodes of starting indexes
    """
    starting_indexes = _get_possible_indexes(start_row, coins[0])
    if len(coins) > 1:
        for i in range(1, len(coins)):
            if not _is_reachable(coins[i - 1], coins[i]):
                return []
    return list(starting_indexes)


def _find_all_paths_with_coins(start_node, coins, end_row, current_path=[], all_paths=[], target_coin=None):
    """
    Method that finds all paths for given coins
    :param start_node: starting node
    :param coins: array of coins
    :param end_row: ending row
    :param current_path: current path
    :param all_paths: all paths until now
    :param target_coin: next coin to collect
    :return: paths for given coins
    """
    current_path.append(start_node)

    if ((start_node[0] == end_row and target_coin is None) or (start_node[0] == end_row and target_coin[0] == end_row))\
            and not (start_node[0] in [coin[0] for coin in coins] and start_node not in coins):
        all_paths.append(current_path.copy())

    else:
        target_coin = None
        for coin in coins:
            if start_node[0] > coin[0]:
                target_coin = coin
                break

        if target_coin is not None:
            if not _is_reachable(start_node, target_coin) or (start_node[0] in [coin[0] for coin in coins]
                                                              and start_node not in coins):
                # it's impossible to get from current node to target node
                return

            else:
                # go to left child
                _find_all_paths_with_coins((start_node[0] - 1, start_node[1]),
                                           coins, end_row, current_path, all_paths, target_coin)
                current_path.remove((start_node[0] - 1, start_node[1]))

                # go to right child
                _find_all_paths_with_coins((start_node[0] - 1, start_node[1] + 1),
                                           coins, end_row, current_path, all_paths, target_coin)
                current_path.remove((start_node[0] - 1, start_node[1] + 1))

        else:
            if start_node[0] < coins[-1][0] or start_node in coins:
                # go to left child
                _find_all_paths_with_coins((start_node[0] - 1, start_node[1]),
                                           coins, end_row, current_path, all_paths, None)
                current_path.remove((start_node[0] - 1, start_node[1]))

                # go to right child
                _find_all_paths_with_coins((start_node[0] - 1, start_node[1] + 1),
                                           coins, end_row, current_path, all_paths, None)
                current_path.remove((start_node[0] - 1, start_node[1] + 1))
            else:
                pass

    return all_paths


def get_all_paths_with_coins(start_row, coins: list, end_row=1):
    """
    Wrapper for _find_all_paths_with_coins method
    :param start_row: starting row
    :param coins: Array of coins
    :param end_row: Ending row, 1 is default
    :return: paths
    """
    if start_row < 1:
        raise ValueError('Start row must be greater than 0')

    if len(coins) == 0:
        raise ValueError('There can\'t be no coins')

    if any(coin[0] > start_row for coin in coins):
        raise ValueError('Start row must be higher than all the coins')

    coins.sort(reverse=True)

    start_indexes = _find_starting_indexes(start_row, coins)

    if not start_indexes:
        raise ValueError('There are no possible starting indexes')

    all_paths = []

    for start_node in start_indexes:
        all_paths.extend(_find_all_paths_with_coins((start_row, start_node), coins, end_row, [], [], None))

    if all_paths is None:
        raise Exception('No paths found!')

    return all_paths


def _calculate_position(point):
    """
    Calculates position of a point to draw a dot on figure
    :param point: point
    :return: position
    """
    return[point[0] - 1 + (point[1] - 1) * 2, point[0] - 1]


def _draw_points_normal_triangle(points, plot, color='g'):
    """ Method that draws points and adds labels based on points argument
    :param points: array of tuples containing point indexes
    :param color: color of points
    :return: dictionary containing index tuples as keys and coordinates as values
    """

    for point in points:
        position = _calculate_position(point)
        plot.scatter(position[0], position[1], 100, c=color)
        text_position = [position[0] - 0.25, position[1] + 0.1]
        plot.text(text_position[0], text_position[1], point, fontsize=7)


def _draw_points_upside_down_triangle(points, plot, color='b'):
    """ Method that draws points and adds labels based on points argument
        :param points: array of tuples containing point indexes
        :param color: color of points
        :return: dictionary containing index tuples as keys and coordinates as values
        """

    for point in points:
        position = _calculate_position(point)
        plot.scatter(position[0], position[1], 100, c=color)
        text_position = [position[0] - 0.25, position[1] + 0.1]
        plot.text(text_position[0], text_position[1], point, fontsize=7)


def _generate_points_upside_down_triangle(point_node, height, plot):
    """ Method that generates triangle indexes as tuples and draws them
        :param point_node: Index of the triangle point
        :param height: Height of the triangle
        """
    points = []
    no_lines = 1
    row = point_node[0]
    while row <= height + point_node[0] - 1:
        points.extend([(row, line - no_lines + 1 + point_node[1]) for line in range(no_lines)])
        no_lines += 1
        row += 1
    _draw_points_upside_down_triangle(points, plot)


def _generate_points_normal_triangle(point_node, plot):
    """ Method that generates triangle indexes as tuples and draws them
    :param point_node: Index of the triangle point
    """
    points = []
    no_lines = 1
    row = point_node[0]
    while row >= 1:
        points.extend([(row, line + point_node[1]) for line in range(no_lines)])
        no_lines += 1
        row -= 1
    _draw_points_normal_triangle(points, plot)


def _generate_triangle_and_neighbour_triangles(start_node, plot):
    """ Method that generates a triangle containing start_node and 2 neighbour triangles
    :param start_node: Starting node
    """
    # If the first index is odd, draw an upside down triangle with point in top line
    if start_node[0] % 2 == 1:

        # find the end of the top line
        point = start_node[1]
        try:
            while point % (start_node[0] - 1) != 0:
                point += 1
        except ZeroDivisionError:
            point += 1
        _generate_points_upside_down_triangle((2, point), start_node[0] - 2 + 1, plot)

        # check if left triangle doesn't contain negative values
        if point - start_node[0] + 2 > 0:
            _generate_points_normal_triangle((start_node[0] - 1, point - start_node[0] + 2), plot)
        # generate right triangle
        _generate_points_normal_triangle((start_node[0] - 1, point + 1), plot)

    # If the first index is even and first % second = 1, point is top of a normal triangle
    elif start_node[1] % start_node[0] == 1:
        _generate_points_normal_triangle(start_node, plot)

        # check if left triangle doesn't contain negative values
        if start_node[1] - 1 > 0:
            _generate_points_upside_down_triangle((2, start_node[1] - 1), start_node[0] - 2 + 2, plot)
        # generate right triangle
        _generate_points_upside_down_triangle((2, start_node[1] + 1 + (start_node[0] - 2)), start_node[0] - 2 + 2, plot)

    # The point is inside an upside down triangle
    else:
        # find the end of the top line
        point = start_node[1]
        while point % (start_node[0]) != 0:
            point += 1
        _generate_points_upside_down_triangle((2, point), start_node[0] - 1 + 1, plot)

        # check if left triangle doesn't contain negative values
        if point - start_node[0] + 1 > 0:
            _generate_points_normal_triangle((start_node[0], point - start_node[0] + 1), plot)
        # generate right triangle
        _generate_points_normal_triangle((start_node[0], point + 1), plot)


def draw_path(path):
    """ Method that draws paths
    :param path: Path to draw
    """
    figure = plt.figure(figsize=(10, 9), dpi=100)
    plot = figure.add_subplot(1, 1, 1)
    _generate_triangle_and_neighbour_triangles(path[0], plot)
    for i in range(1, len(path)):
        position1 = _calculate_position(path[i - 1])
        position2 = _calculate_position(path[i])
        plot.arrow(position1[0], position1[1], position2[0] - position1[0], position2[1] - position1[1])
    plot.axis('off')
    return figure


def draw_all_paths(paths):
    """ Method that draws all paths
       :param paths: Paths to draw
       """
    figure = plt.figure(figsize=(10, 9), dpi=100)
    plot = figure.add_subplot(1, 1, 1)
    _generate_triangle_and_neighbour_triangles(paths[0][0], plot)
    for path in paths:
        for i in range(1, len(path)):
            position1 = _calculate_position(path[i - 1])
            position2 = _calculate_position(path[i])
            plot.arrow(position1[0], position1[1], position2[0] - position1[0], position2[1] - position1[1])
    plot.axis('off')
    return figure


def draw_path_with_coins(path, coins):
    """ Method that draws all paths
           :param path: Path to draw
           :param coins: Coins to draw
           """
    figure = plt.figure(figsize=(10, 9), dpi=100)
    plot = figure.add_subplot(1, 1, 1)
    _generate_triangle_and_neighbour_triangles(path[0], plot)
    for coin in coins:
        position = _calculate_position(coin)
        plot.scatter(position[0], position[1], 100, c='y', marker='D')
    for i in range(1, len(path)):
        position1 = _calculate_position(path[i - 1])
        position2 = _calculate_position(path[i])
        plot.arrow(position1[0], position1[1], position2[0] - position1[0], position2[1] - position1[1])
    plot.axis('off')
    return figure


def draw_all_paths_with_coins(paths, coins):
    """ Method that draws all paths
           :param paths: Paths to draw
           :param coins: Coins to draw
           """
    figure = plt.figure(figsize=(10, 9), dpi=100)
    plot = figure.add_subplot(1, 1, 1)
    _generate_triangle_and_neighbour_triangles(paths[0][0], plot)
    for coin in coins:
        position = _calculate_position(coin)
        plot.scatter(position[0], position[1], 100, c='y', marker='D')
    for path in paths:
        for i in range(1, len(path)):
            position1 = _calculate_position(path[i - 1])
            position2 = _calculate_position(path[i])
            plot.arrow(position1[0], position1[1], position2[0] - position1[0], position2[1] - position1[1])
    plot.axis('off')
    return figure
