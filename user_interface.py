import tkinter as tk
import methods
from ast import literal_eval
from threading import Thread
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import sys


def _get_coins_input(coins_frame):
    coins = []
    for child in coins_frame.winfo_children():
        if len(child.get()) == 0:
            _create_popup_with_ok_button('Error', "Please specify all coins")
        coins.append(tuple(literal_eval(child.get())))
    return coins


def _get_input(input_box):
    if len(input_box.get()) == 0:
        _create_popup_with_ok_button('Error', "Input can't be empty")
    return literal_eval(input_box.get())


def _create_popup(title, text):
    popup = tk.Toplevel()
    popup.resizable(False, False)
    popup.wm_title(title)
    popup.geometry("250x50")
    label = tk.Label(popup, text=text)
    label.pack()
    popup.grab_set()
    popup.update()
    return popup


def _create_popup_with_ok_button(title, text):
    popup = tk.Toplevel()
    popup.resizable(False, False)
    popup.wm_title(title)
    popup.geometry("250x75")
    label = tk.Label(popup, text=text)
    button = tk.Button(popup, text='Ok', command=popup.destroy)
    label.pack()
    button.pack()
    popup.grab_set()
    popup.update()
    return popup


def _destroy_popup(popup):
    popup.destroy()
    popup.grab_release()


def _on_frame_configure(canvas):
    canvas.configure(scrollregion=canvas.bbox("all"))


def _quit(tk_window):
    tk_window.quit()
    tk_window.destroy()


def _draw_paths_with_coins(window, path, coins):
    popup = _create_popup('Drawing', 'Drawing path...')
    if len(window.winfo_children()) > 1:
        for w in window.winfo_children()[1:]:
            w.destroy()

    right_frame = tk.Frame(master=window)

    thread = ThreadWithReturnValue(target=methods.draw_path_with_coins, args=(path, coins,))
    thread.start()

    figure = thread.join()

    canvas = FigureCanvasTkAgg(figure, master=right_frame)
    canvas.draw()
    canvas.get_tk_widget().pack()

    right_frame.grid(row=0, column=1)
    _destroy_popup(popup)


def _draw_all_paths_with_coins(window, paths, coins):
    popup = _create_popup('Drawing', 'Drawing paths...')
    if len(window.winfo_children()) > 1:
        for w in window.winfo_children()[1:]:
            w.destroy()

    right_frame = tk.Frame(master=window)

    thread = ThreadWithReturnValue(target=methods.draw_all_paths_with_coins, args=(paths, coins,))
    thread.start()

    figure = thread.join()

    canvas = FigureCanvasTkAgg(figure, master=right_frame)
    canvas.draw()
    canvas.get_tk_widget().pack()

    right_frame.grid(row=0, column=1)
    _destroy_popup(popup)


def _draw_paths(window, path):
    popup = _create_popup('Drawing', 'Drawing path...')
    if len(window.winfo_children()) > 1:
        for w in window.winfo_children()[1:]:
            w.destroy()

    right_frame = tk.Frame(master=window)

    thread = ThreadWithReturnValue(target=methods.draw_path, args=(path,))
    thread.start()

    figure = thread.join()

    canvas = FigureCanvasTkAgg(figure, master=right_frame)
    canvas.draw()
    canvas.get_tk_widget().pack()

    right_frame.grid(row=0, column=1)
    _destroy_popup(popup)


def _draw_all_paths(window, paths):
    popup = _create_popup('Drawing', 'Drawing paths...')
    if len(window.winfo_children()) > 1:
        for w in window.winfo_children()[1:]:
            w.destroy()

    right_frame = tk.Frame(master=window)

    thread = ThreadWithReturnValue(target=methods.draw_all_paths, args=(paths,))
    thread.start()

    figure = thread.join()

    canvas = FigureCanvasTkAgg(figure, master=right_frame)
    canvas.draw()
    canvas.get_tk_widget().pack()

    right_frame.grid(row=0, column=1)

    _destroy_popup(popup)


def _generate_all_paths(left_frame, start_node, end_node, window):
    if type(start_node) is not tuple or len(start_node) != 2:
        _create_popup_with_ok_button('Error', "Starting index not valid")
        return
    if type(end_node) is not tuple or len(end_node) != 2:
        _create_popup_with_ok_button('Error', "Ending index not valid")
        return
    if start_node[0] < end_node[0]:
        _create_popup_with_ok_button('Error', "Ending index can't be \n in a higher row than the starting index")
        return

    thread = ThreadWithReturnValue(target=methods.get_all_paths, args=(start_node, end_node,))

    thread.start()
    all_paths = thread.join()

    _draw_all_paths(window, all_paths)

    window.update_idletasks()


def _generate_paths(left_frame, start_node, end_node, window):
    if type(start_node) is not tuple or len(start_node) != 2:
        _create_popup_with_ok_button('Error', "Starting index not valid")
        return
    if type(end_node) is not tuple or len(end_node) != 2:
        _create_popup_with_ok_button('Error', "Ending index not valid")
        return
    if start_node[0] < end_node[0]:
        _create_popup_with_ok_button('Error', "Ending index can't be \n in a higher row than the starting index")
        return

    thread = ThreadWithReturnValue(target=methods.get_all_paths, args=(start_node, end_node,))
    thread.start()

    all_paths = thread.join()
    if all_paths is None:
        return

    if len(all_paths) > 1000:
        _create_popup_with_ok_button('Warning', '{} paths found, \n only 1000 will be shown.'.format(len(all_paths)))

    if len(left_frame.winfo_children()) > 1:
        for w in left_frame.winfo_children()[1:]:
            w.destroy()

    parent_frame = tk.Frame(master=left_frame, background='#A8D3EE')
    parent_frame.grid_rowconfigure(0, weight=1)
    parent_frame.grid_columnconfigure(0, weight=1)

    paths_and_scroll_canvas = tk.Canvas(master=parent_frame, background='#A8D3EE', height=600)

    paths_and_scroll_frame = tk.Frame(master=paths_and_scroll_canvas, background='#A8D3EE')

    paths_frame = tk.Frame(master=paths_and_scroll_frame, background='#A8D3EE')

    for i, path in enumerate(all_paths[:1000]):
        paths_sb = ''
        for node in path:
            paths_sb += str(node) + ' '
        button = tk.Button(master=paths_frame, text=paths_sb)
        button.config(command=lambda text=path: _draw_paths(window, text))
        button.grid(row=i, pady=1)
    paths_frame.pack(side='left', fill='both')

    scrollbar = tk.Scrollbar(parent_frame, orient="vertical", command=paths_and_scroll_canvas.yview)
    paths_and_scroll_canvas.configure(yscrollcommand=scrollbar.set)

    scrollbar.pack(side="right", fill="y")
    paths_and_scroll_canvas.pack(side="left", fill="both", expand=True)
    paths_and_scroll_canvas.create_window((0, 0), window=paths_and_scroll_frame, anchor='nw')
    parent_frame.grid(row=1, pady=10, sticky='nsew')
    paths_and_scroll_frame.bind("<Configure>",
                                lambda event, canvas=paths_and_scroll_canvas: _on_frame_configure(canvas))

    window.update_idletasks()


def _generate_paths_with_coins(left_frame, start_row, coins, window):
    thread = ThreadWithReturnValue(target=methods.get_all_paths_with_coins, args=(start_row, coins))
    thread.start()

    all_paths = thread.join()
    if all_paths is None:
        return

    if len(all_paths) > 1000:
        _create_popup_with_ok_button('Warning', '{} paths found, \n only 1000 will be shown.'.format(len(all_paths)))

    if len(left_frame.winfo_children()) > 1:
        for w in left_frame.winfo_children()[1:]:
            w.destroy()

    parent_frame = tk.Frame(master=left_frame, background='#A8D3EE')
    parent_frame.grid_rowconfigure(0, weight=1)
    parent_frame.grid_columnconfigure(0, weight=1)

    paths_and_scroll_canvas = tk.Canvas(master=parent_frame, background='#A8D3EE', height=600)

    paths_and_scroll_frame = tk.Frame(master=paths_and_scroll_canvas, background='#A8D3EE')

    paths_frame = tk.Frame(master=paths_and_scroll_frame, background='#A8D3EE')

    for i, path in enumerate(all_paths[:1000]):
        paths_sb = ''
        for node in path:
            paths_sb += str(node) + ' '
        button = tk.Button(master=paths_frame, text=paths_sb)
        button.config(command=lambda text=path: _draw_paths_with_coins(window, text, coins))
        button.grid(row=i, pady=1)
    paths_frame.pack(side='left', fill='both')

    scrollbar = tk.Scrollbar(parent_frame, orient="vertical", command=paths_and_scroll_canvas.yview)
    paths_and_scroll_canvas.configure(yscrollcommand=scrollbar.set)

    scrollbar.pack(side="right", fill="y")
    paths_and_scroll_canvas.pack(side="left", fill="both", expand=True)
    paths_and_scroll_canvas.create_window((0, 0), window=paths_and_scroll_frame, anchor='nw')
    parent_frame.grid(row=1, pady=10, sticky='nsew')
    paths_and_scroll_frame.bind("<Configure>",
                                lambda event, canvas=paths_and_scroll_canvas: _on_frame_configure(canvas))

    window.update_idletasks()


def _generate_all_paths_with_coins(left_frame, start_row, coins, window):
    thread = ThreadWithReturnValue(target=methods.get_all_paths_with_coins, args=(start_row, coins))

    thread.start()
    all_paths = thread.join()

    _draw_all_paths_with_coins(window, all_paths, coins)

    window.update_idletasks()


def init_input_frame(input_frame, window, left_frame):
    tk.Button(master=input_frame, text="Quit", command=lambda: _quit(window)).grid(row=5, column=1, padx=5, pady=5)
    tk.Label(master=input_frame, text="Starting index", background='#A8D3EE').grid(row=1)
    tk.Label(master=input_frame, text="End index", background='#A8D3EE').grid(row=2)

    start_node_input = tk.Entry(master=input_frame)
    end_node_input = tk.Entry(master=input_frame)

    start_node_input.grid(row=1, column=1)
    end_node_input.grid(row=2, column=1)

    tk.Button(master=input_frame, text="Draw all paths",
              command=lambda: _generate_all_paths(left_frame, _get_input(start_node_input),
                                                  _get_input(end_node_input), window)).grid(row=3, column=1, padx=5,
                                                                                            pady=5)

    tk.Button(master=input_frame, text="Draw paths",
              command=lambda: _generate_paths(left_frame, _get_input(start_node_input),
                                              _get_input(end_node_input), window)).grid(row=4, column=1, padx=5, pady=5)

    checkbox = tk.BooleanVar()
    tk.Checkbutton(master=input_frame, text="Use coins", variable=checkbox, onvalue=True, offvalue=False,
                   command=lambda: _checkbox_command(checkbox, input_frame, window, left_frame),
                   background='#A8D3EE').grid(row=0, column=0, padx=5, pady=5)

    input_frame.grid(row=0, sticky='nsew')

    left_frame.grid(row=0, column=0, sticky='nsew')


def _add_a_coin(coins_frame):
    tk.Entry(master=coins_frame).pack(padx=1, pady=1)


def _remove_a_coin(coins_frame):
    if len(coins_frame.winfo_children()) > 1:
        coins_frame.winfo_children()[-1].destroy()


def init_input_frame_with_coins(input_frame, window, left_frame):
    tk.Button(master=input_frame, text="Quit", command=lambda: _quit(window)).grid(row=7, column=1, padx=5, pady=5)
    tk.Label(master=input_frame, text="Starting row", background='#A8D3EE').grid(row=1)
    coins_frame = tk.Frame(input_frame)
    tk.Label(master=input_frame, text="Coins", background='#A8D3EE').grid(row=2)

    start_row_input = tk.Entry(master=input_frame)
    coins_input = tk.Entry(master=coins_frame)

    start_row_input.grid(row=1, column=1, pady=7)
    coins_input.pack()
    tk.Button(master=input_frame, text="Add a coin", command=lambda: _add_a_coin(coins_frame)).grid(row=3, column=1,
                                                                                                    padx=5, pady=5)
    tk.Button(master=input_frame, text="Remove a coin", command=lambda: _remove_a_coin(coins_frame)).grid(row=4,
                                                                                                          column=1,
                                                                                                          padx=5,
                                                                                                          pady=5)
    tk.Button(master=input_frame, text="Draw all paths with coins",
              command=lambda: _generate_all_paths_with_coins(left_frame, _get_input(start_row_input),
                                                             _get_coins_input(coins_frame), window)).grid(row=5,
                                                                                                          column=1,
                                                                                                          padx=5,
                                                                                                          pady=5)

    tk.Button(master=input_frame, text="Draw paths with coins",
              command=lambda: _generate_paths_with_coins(left_frame, _get_input(start_row_input),
                                                         _get_coins_input(coins_frame), window)).grid(row=6, column=1,
                                                                                                      padx=5, pady=5)

    coins_frame.grid(row=2, column=1)

    input_frame.grid(row=0, sticky='nsew')

    left_frame.grid(row=0, column=0, sticky='nsew')


def _checkbox_command(checkbox, input_frame, window, left_frame):
    if checkbox.get():
        if len(input_frame.winfo_children()) > 1:
            for w in input_frame.winfo_children()[:-1]:
                w.destroy()
        if len(left_frame.winfo_children()) > 1:
            left_frame.winfo_children()[1].destroy()
        init_input_frame_with_coins(input_frame, window, left_frame)
    else:
        for w in input_frame.winfo_children():
            w.destroy()
        if len(left_frame.winfo_children()) > 1:
            left_frame.winfo_children()[1].destroy()
        init_input_frame(input_frame, window, left_frame)


def generate_user_interface():
    window = tk.Tk()
    window.wm_title("Vizualizacija kombinatornih baza standardnih modula simplektičkih afinih Liejevih algebri")
    window.configure(background='#A8D3EE')
    window.state('zoomed')
    window.grid_rowconfigure(0, weight=1)
    window.grid_columnconfigure(0, weight=1)

    left_frame = tk.Frame(window, background='#A8D3EE')

    input_frame = tk.Frame(left_frame, background='#A8D3EE')
    init_input_frame(input_frame, window, left_frame)
    window.protocol("WM_DELETE_WINDOW", sys.exit)

    window.mainloop()


class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args,
                                        **self._kwargs)

    def join(self, *args):
        loading_popup = _create_popup('Loading', 'Calculating...')
        Thread.join(self, *args)
        _destroy_popup(loading_popup)
        if self._return is None:
            _create_popup_with_ok_button('Error', 'No paths found!')
        return self._return


if __name__ == "__main__":
    generate_user_interface()
